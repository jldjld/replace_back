<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/product", name="api_product")
 */
class ApiProductController extends AbstractController {


    /**
     * @Route(methods="GET")
     */
    public function getAll(ProductRepository $repo)
    {
        return $this->json($repo->findBy([]));
    }
    /**
     * @Route("/{sustainable}", methods="GET")
     */
    public function getSustainableProducts(string $sustainable, ProductRepository $repo)
    {
        return $this->json($repo->findBy(['sustainable' => $sustainable]));
    }
    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(Product $product)
    {
        return $this->json($product);
    }
    /**
     * @Route("/categories/{category}", methods="GET")
     */
    public function searchByCategory(string $category, ProductRepository $repo){
        
        return $this->json($repo->findBy(['category' => $category]));
    }
    /**
     * @Route("/search/{name}", methods="GET")
     */
    public function searchByName(string $name, ProductRepository $repo){
        return $this->json($repo->findByName($name));
    }
}
