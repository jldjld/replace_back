<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestApiController extends AbstractController
{
    /**
     * @Route("/test/api", name="test_api")
     */
    public function index()
    {
        return $this->render('test_api/index.html.twig', [
            'controller_name' => 'TestApiController',
        ]);
    }
}
