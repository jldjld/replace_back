<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\TestimoniesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/{search}", methods="GET")
     */
    public function search(string $search, ProductRepository $repo)
    {
        return $this->json($repo->findByName($search));
    }
    /**
     * @Route("/testimonies/{testimonies}", methods="GET")
     */
    public function accessTestimonies(string $testimonies, TestimoniesRepository $repo){
        return $this->json($repo->findbyName($testimonies));
    }
}
