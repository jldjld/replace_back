<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
class ProductsFixtures extends Fixture
{
        public function load(ObjectManager $manager)
        {
                

                $cleanablewipper = new Product();
                $cleanablewipper
                ->setName('Lingettes lavables')
                ->setSustainable(true)
                ->setAltImgPath("assets/images/sustWipes.jpg")
                ->setDescription("Lingettes lavables en tissus")
                ->setCategory('cuisine')
                ->setdecayingTime(100)
                ->setUsingTime(100);
                $manager->persist($cleanablewipper);
                $manager->flush($cleanablewipper);
        

        $markerAlternative = new Product();
        $markerAlternative
        ->setName('marqueur rechargeable')
        ->setSustainable(true)
        ->setAltImgPath("assets/images/sustMarker.jpg")
        ->setDescription("Marqueur - non permanent - pour tableau blanc - bleu - encre à base d'alcool")
        ->setCategory('bureau')
        ->setdecayingTime(200)
        ->setUsingTime(10);
        $manager->persist($markerAlternative);
        $manager->flush($markerAlternative);

        $fillableInk = new Product();
            $fillableInk
                    ->setName("cartouches d'encres rechargeables")
                    ->setSustainable(true)
                    ->setAltImgPath("assets/images/sustInk.jpg")
                    ->setDescription("Plus économiques et plus écologiques")
                    ->setCategory('bureau')
                    ->setdecayingTime(20)
                    ->setUsingTime(10);
            $manager->persist($fillableInk);
            $manager->flush($fillableInk);

            $sustPostIt = new Product();
            $sustPostIt
                    ->setName("Post-it")
                    ->setSustainable(true)
                    ->setAltImgPath("assets/images/sustPostIt.jpg")
                    ->setDescription("Mémos repositionnables, réutilisables et effaçables à volonté ! Adhère sans colle.Notez une idée, un numéro, un rappel.Effacez sans efforts à sec ou à l’eau… déplacez etréutilisez à l’infini !")
                    ->setCategory('bureau')
                    ->setdecayingTime(12)
                    ->setUsingTime(2);
            $manager->persist($sustPostIt);
            $manager->flush($sustPostIt);

            $reUseCup = new Product();
            $reUseCup
                    ->setName("Bouteille réutilisable")
                    ->setSustainable(true)
                    ->setAltImgPath("assets/images/bouteille-personnalisable.jpg")
                    ->setDescription("Le gobelet à usage unique est une source de déchet considérable. Une solution plus durable et économique.")
                    ->setCategory('cuisine')
                    ->setdecayingTime(12)
                    ->setUsingTime(1);
            $manager->persist($reUseCup);
            $manager->flush($reUseCup);

            $marker = new Product();
            $marker
                    ->setName('marqueur tableau')
                    ->setSustainable(false)
                    ->setBpImgPath("assets/images/bic.jpg")
                    ->setDescription("Marqueur - non permanent - pour tableau blanc - bleu - encre à base d'alcool")
                    ->setCategory('bureau')
                    ->setdecayingTime(100)
                    ->setUsingTime(1)
                    ->addAlternative($markerAlternative);
                    $manager->persist($marker);
                    $manager->flush($marker);

            $priceyInk = new Product();
            $priceyInk
                    ->setName("cartouches d'encres")
                    ->setSustainable(false)
                    ->setBpImgPath("assets/images/bpInk.jpg")
                    ->setDescription("Cette Cartouche d'encre multi pacea")
                    ->setCategory('bureau')
                    ->setdecayingTime(100)
                    ->setUsingTime(2)
                    ->addAlternative($fillableInk);
            $manager->persist($priceyInk);
            $manager->flush($priceyInk);
    
            
    
            $PostIt = new Product();
            $PostIt
                    ->setName("Post-it")
                    ->setSustainable(false)
                    ->setBpImgPath("assets/images/bpPostIt.jpg")
                    ->setDescription("Une méthode simple pour laisser un message, dresser une liste ou coucher une idée sur papier. Faites votre choix au sein d'un assortiment de blocs-notes aux teintes et dimensions étonnantes.")
                    ->setCategory('bureau')
                    ->setdecayingTime(6)
                    ->setUsingTime(1);
            $manager->persist($PostIt);
            $manager->flush($PostIt);
    
            $cup = new Product();
            $cup
                    ->setName("Gobelet en plastique")
                    ->setSustainable(false)
                    ->setBpImgPath("assets/images/bpCup.jpg")
                    ->setDescription("Gobelets a usage unique")
                    ->setCategory('cuisine')
                    ->setdecayingTime(12)
                    ->setUsingTime(1)
                    ->addAlternative($reUseCup);
            $manager->persist($cup);
            $manager->flush($cup);

        
            $wippers = new Product();
            $wippers
            ->setName('Sopalin')
            ->setSustainable(false)
            ->setBpImgPath("assets/images/wipes.jpg")
            ->setDescription("Papier blanchi au chlore, à usage unique")
            ->setCategory('cuisine')
            ->setdecayingTime(100)
            ->setUsingTime(100)
            ->addAlternative($cleanablewipper);
            $manager->persist($wippers);
            $manager->flush($wippers);

    
            
        }
    }