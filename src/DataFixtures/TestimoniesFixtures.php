<?php

namespace App\DataFixtures;

use App\Entity\Testimonies as EntityTestimonies;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TestimoniesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $greenBodyShop = new EntityTestimonies();
        $greenBodyShop
            ->setTitle("Green Body Shop, vers un managment éco-responsable ")
            ->setContent("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse animi distinctio magni, illum, qui beatae molestiae recusandae aliquid ea in maiores. Obcaecati esse adipisci autem impedit quas ipsa ad aliquam!
            ")
            ->setImgPath("assets/images/article1.jpg");
            $manager->persist($greenBodyShop);
            $manager->flush($greenBodyShop);
        
            $naele = new EntityTestimonies();
            $naele
                ->setTitle("Elena Naele, 5 outils pour mettre en place le zéro-déchet en entreprise")
                ->setContent("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse animi distinctio magni, illum, qui beatae molestiae recusandae aliquid ea in maiores. Obcaecati esse adipisci autem impedit quas ipsa ad aliquam!
                ")
                ->setImgPath("assets/images/article2.jpg");
                $manager->persist($naele);
                $manager->flush($naele);

                $conseil = new EntityTestimonies();
                $conseil
                    ->setTitle("Matériel de bureau, investir pour moins jeter")
                    ->setContent("Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse animi distinctio magni, illum, qui beatae molestiae recusandae aliquid ea in maiores. Obcaecati esse adipisci autem impedit quas ipsa ad aliquam!
                    ")
                    ->setImgPath("assets/images/article3.jpg");
                    $manager->persist($conseil);
                    $manager->flush($conseil);
    

        $manager->flush();
    }
}
