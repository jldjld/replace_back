<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sustainable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $decayingTime;

    /**
     * @ORM\Column(type="integer")
     */
    private $usingTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bpImgPath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $altImgPath;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="favorites")
     */
    private $userFavorites;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product")
     */
    private $alternatives;



    public function __construct()
    {
        $this->userFavorites = new ArrayCollection();
        $this->alternatives = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSustainable(): ?bool
    {
        return $this->sustainable;
    }

    public function setSustainable(bool $sustainable): self
    {
        $this->sustainable = $sustainable;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDecayingTime(): ?int
    {
        return $this->decayingTime;
    }

    public function setDecayingTime(int $decayingTime): self
    {
        $this->decayingTime = $decayingTime;

        return $this;
    }

    public function getUsingTime(): ?int
    {
        return $this->usingTime;
    }

    public function setUsingTime(int $usingTime): self
    {
        $this->usingTime = $usingTime;

        return $this;
    }

    public function getBpImgPath(): ?string
    {
        return $this->bpImgPath;
    }

    public function setBpImgPath(?string $bpImgPath): self
    {
        $this->bpImgPath = $bpImgPath;

        return $this;
    }

    public function getAltImgPath(): ?string
    {
        return $this->altImgPath;
    }

    public function setAltImgPath(?string $altImgPath): self
    {
        $this->altImgPath = $altImgPath;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserFavorites(): Collection
    {
        return $this->userFavorites;
    }

    public function addUserFavorite(User $userFavorite): self
    {
        if (!$this->userFavorites->contains($userFavorite)) {
            $this->userFavorites[] = $userFavorite;
            $userFavorite->addFavorite($this);
        }

        return $this;
    }

    public function removeUserFavorite(User $userFavorite): self
    {
        if ($this->userFavorites->contains($userFavorite)) {
            $this->userFavorites->removeElement($userFavorite);
            $userFavorite->removeFavorite($this);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getAlternatives(): Collection
    {
        return $this->alternatives;
    }

    public function addAlternative(self $alternative): self
    {
        if (!$this->alternatives->contains($alternative)) {
            $this->alternatives[] = $alternative;
        }

        return $this;
    }

    public function removeAlternative(self $alternative): self
    {
        if ($this->alternatives->contains($alternative)) {
            $this->alternatives->removeElement($alternative);
        }

        return $this;
    }
}
